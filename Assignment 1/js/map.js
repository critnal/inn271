/*
This function checks which page the user is on and styles the 
corresponding navigation menu button appropriately
*/
function initialiseMap() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
        center: new google.maps.LatLng(44.5403, -78.5463),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions)

    var location = new google.maps.LatLng(-27.56244221, 153.0809183);
    var mapMarker = new google.maps.Marker({
        position: location,
        title: "Garden City Shopping Centre"
    });

    mapMarker.setMap(map);
    map.setCenter(location);
}
google.maps.event.addDomListener(window, 'load', initialiseMap);