<?php

require_once 'global-utilities.php';
require_once 'sql-utilities.php';
require_once 'option-lists.php';

function getReviewsForItem($itemID) {
	$reviews = array();
	
	$pdo = getNewPDO();
	$query = $pdo->prepare(SELECT_ITEM_REVIEWS);
	$query->bindValue(':itemid', $itemID);
	$query->execute();
	
	for ($i = 0; $i < $query->rowCount(); $i++) {
		$reviews[$i] = $query->fetch();
	}
	
	return $reviews;
}

function getRecentReviews() {
	$reviews = array();
	
	$pdo = getNewPDO();
	$query = $pdo->prepare(SELECT_ALL_REVIEWS);
	$query->execute();
	
	for ($i = 0; $i < $query->rowCount(); $i++) {
		$reviews[$i] = $query->fetch();
	}
	
	return $reviews;
}

function showReviews($reviews) {
	foreach ($reviews as $review) {
		echo review(
			ucwords(strtolower($review['name'])),
			$review['content'],
			getItemRating($review['rating']),
			'item.php?id=' . $review['itemid']
		);
	}
	if (count($reviews) <= 0) {
		echo '<h1>Be the first to write a review!</h2>';
	}
}

function review($heading, $text, $rating, $link) {
	$reviewContent = '
		<div class="review-content">
			<div class="text">
				' . $text . '
			</div>
			<div class="rating">
				' . $rating . '
			</div>
		</div>
	';	
	return genericLink($heading, $reviewContent, $link, '', '');
}

function validateReview($username, $itemID, $content, &$errors) {
	if ($content == '') {
		$errors['content'] = 1;
	}
	
	return count($errors) <= 0;
}

function enterReview($username, $itemID, $content, $rating) {
	$pdo = getNewPDO();
	$query = $pdo->prepare(INSERT_NEW_REVIEW);
	$query->bindValue(':itemid', $itemID);
	$query->bindValue(':username', $username);
	$query->bindValue(':content', $content);
	$query->bindValue(':rating', $rating);	
	doQuery($query);
	
	$query = $pdo->prepare(SELECT_ITEM_ALL);
	$query->bindValue(':id', $itemID);
	doQuery($query);
	
	$item = $query->fetch();
	$oldRating = $item['rating'];
	$ratingCount = $item['ratingcount'];
	$newRating = (($oldRating * $ratingCount) + $rating) / ($ratingCount + 1);
	
	$query = $pdo->prepare(UPDATE_ITEM_RATING);
	$query->bindValue(':newrating', $newRating);
	$query->bindValue(':newratingcount', ($ratingCount + 1));
	$query->bindValue(':id', $itemID);
	doQuery($query);
	
}

?>