<?php

session_start();

storeSuburbs();	

function storeSuburbs() {
	$pdo = getNewPDO();
	$query = $pdo->prepare(SELECT_SUBURBS);
	$query->execute();
	
	$suburbs = array('Any' => 'Any');
	$length = count($suburbs) + $query->rowCount();
	for ($i = count($suburbs); $i < $length; $i++) {
		$suburb = $query->fetch()['suburb'];
		$suburbs[$suburb] = $suburb;
	}
	$_SESSION['suburb-options'] = $suburbs;
}

function getSuburbOptions() {
	return $_SESSION['suburb-options'];
}

function getDistanceOptions() {
	return array(
		'Any' => 'Any',	
		'< 1km' => 1,		
		'< 5km' => 5,		
		'< 10km' => 10,	
		'< 20km' => 20,	
		'< 40km' => 40 	
	);
}

function getRatingOptions() {
	return array(
		'Any' => 0,
		'Adequate' => 1.5,
		'Elegant' => 2.5		
	);
}

function getRatingComparison() {
	return array(
		'Inadequate' => 0,
		'Adequate' => 1.5,
		'Elegant' => 2.5		
	);
}

function getRatingValues() {
	return array(
		'Inadequate' => 1,
		'Adequate' => 2,
		'Elegant' => 3		
	);
}

function getItemRating($rating) {
	foreach (array_reverse(getRatingComparison()) as $key => $value) {
		if ($rating >= $value) {
			return $key;
		}
	}
	return '';
}

?>