<?php

/**
 * General Database Info	
 */
define("DATABASE_NAME", "n6878296");
define("MYSQL_CONNECTION", "mysql:host=localhost;dbname=n6878296");
define("MYSQL_USERNAME", "n6878296");
define("MYSQL_PASSWORD", "6_0ox4sv8LPpBR32");

/**
 * Member Table Statements
 */
define("SELECT_MEMBER_LOGIN", "
	SELECT username, password 
	FROM Members 
	WHERE username = :username 
	AND password = SHA2(CONCAT(:password, salt), 0)
");
define(	"INSERT_NEW_MEMBER","
	INSERT INTO Members (username, password, salt, firstname, lastname, email, dateofbirth, subscribe)
	VALUES (:username, SHA2(CONCAT(:password, :salt), 0), :salt, :firstname, :lastname, :email, :dateofbirth, :subscribe)
");
define("SELECT_EXISTING_MEMBER", "
	SELECT username 
	FROM Members 
	WHERE username = :username
");
define("SELECT_MEMBER_DETAILS", "
	SELECT * 
	FROM Members 
	WHERE username = :username
");

/**
 * Items Table Statements
 */
// define("SELECT_ITEMS_COLUMN_COUNT", "SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '" . DATABASE_NAME . "' AND table_name = 'Items'");
define("CLEAR_ITEMS_TABLE", "
	TRUNCATE TABLE items
");
define("INSERT_NEW_ITEM", "
	INSERT INTO Items (id, name, place, address, suburb, description, disabledaccess, availability, latitude, longitude) 
	VALUES (:id, :name, :place, :address, :suburb, :description, :disabledaccess, :availability, :latitude, :longitude)
");
define("SELECT_SUBURBS", "
	SELECT DISTINCT suburb 
	FROM Items 
	ORDER BY suburb
");
define("SELECT_ITEM_ALL", "
	SELECT *
	FROM Items 
	WHERE id = :id
");
define("UPDATE_ITEM_RATING" ,"
	UPDATE Items
	SET rating = :newrating, ratingcount = :newratingcount
	WHERE id = :id
");

/**
 * Reviews Table Statements
  */
define("SELECT_ITEM_REVIEWS", "
	SELECT r.id, r.itemid, r.username, r.date, r.content, r.rating, i.name
	FROM Reviews r, Items i
	WHERE r.itemid = i.id 
	AND itemid = :itemid
	ORDER BY date DESC
	LIMIT 20
");
define("SELECT_ALL_REVIEWS", "
	SELECT r.id, r.itemid, r.username, r.date, r.content, r.rating, i.name
	FROM Reviews r, Items i
	WHERE r.itemid = i.id
	ORDER BY date DESC
	LIMIT 50
");
define("INSERT_NEW_REVIEW", "
	INSERT INTO Reviews (itemid, username, date, content, rating)
	VALUE (:itemid, :username, NOW(), :content, :rating)
");

function getNewPDO() {
	$pdo = new PDO(MYSQL_CONNECTION, MYSQL_USERNAME, MYSQL_PASSWORD);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $pdo;
}

function doQuery(&$query) {
	try {
		$query->execute();		
	} catch (PDOexception $exception) {
		echo $exception->getMessage();
	}
}

?>