<?php

require_once "sql-utilities.php";

function performLogin() {	
	try {
		$pdo = getNewPDO();		
		$query = $pdo->prepare(SELECT_MEMBER_LOGIN);
		$query->bindValue(":username", $_POST['username']);
		$query->bindValue(":password", $_POST['password']);
		$query->execute();
		
		return $query->rowCount() > 0;
		
	} catch (PDOexception $exception) {
		echo $exception->getMessage();
	}
}

function setUserSession() {	
	endUserSession();
	session_start();
	
	$_SESSION['loggedin'] = true;
	
	$pdo = getNewPDO();		
	
	$query = $pdo->prepare(SELECT_MEMBER_DETAILS);
	$query->bindValue(":username", $_POST['username']);
	$query->execute();
	
	$member = $query->fetch();
	
	$_SESSION['username'] = $member['username'];
	$_SESSION['firstname'] = $member['firstname'];
	$_SESSION['lastname'] = $member['lastname'];
	$_SESSION['email'] = $member['email'];
	$_SESSION['dateofbirth'] = $member['dateofbirth'];
	$_SESSION['subscribe'] = $member['subscribe'];
	$_SESSION['admin'] = $member['admin'];
}

function endUserSession() {
	// unset($_SESSION['loggedin']);
	session_destroy();
}

function performRegistration(&$errors) {
	validateAllFields($errors);
	if (count($errors) <= 0) {
		registerMember($errors);
	}	
}

function validateAllFields(&$errors) {
	$pdo = getNewPDO();	
	$query = $pdo->prepare(SELECT_EXISTING_MEMBER);
	$query->bindValue(":username", $_POST['username']);
	$query->execute();	
	
	if ($query->rowCount() > 0) {
		$errors['usernametaken'] = 1;		
	}
	
	validatePattern($errors, 'username', '/^[a-zA-Z.!@#$%^&*()-_=+]+$/');
	validatePattern($errors, 'firstname', '/^[a-zA-Z]+$/');
	validatePattern($errors, 'lastname', '/^[a-zA-Z]+$/');
	validatePattern($errors, 'email', '/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/');
	validatePattern($errors, 'dateofbirth', '/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/');
	validatePassword($errors, 'password', 'passwordconfirm', '/^[a-zA-Z0-9]+$/');
}

function validatePassword(&$errors, $password, $passwordConfirm, $pattern) {
	if (validatePattern($errors, 'password', $pattern)) {
		if ($_POST[$password] != $_POST[$passwordConfirm]) {
			$errors[$passwordConfirm] = 1;
		}		
	}
}

function validatePattern(&$errors, $fieldName, $pattern) {
	if (!isset($_POST[$fieldName]) || $_POST[$fieldName] == ''
		|| !preg_match($pattern, $_POST[$fieldName])) {
		$errors[$fieldName] = 1;
		return false;
	}
	return true;
}

function registerMember(&$errors) {
	try {
		$pdo = getNewPDO();	
		$query = $pdo->prepare(INSERT_NEW_MEMBER);
		$query->bindValue(":username", $_POST['username']);
		$query->bindValue(":password", $_POST['password']);
		$query->bindValue(":salt", uniqid());
		$query->bindValue(":firstname", $_POST['firstname']);
		$query->bindValue(":lastname", $_POST['lastname']);
		$query->bindValue(":email", $_POST['email']);
		$query->bindValue(":dateofbirth", $_POST['dateofbirth']);
		$query->bindValue(":subscribe", isset($_POST['subscribe']) ? 1 : 0);
		$query->execute();
		
	} catch (PDOexception $exception) {
		echo $exception->getMessage();
	}
}

?>