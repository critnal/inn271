<?php

require_once 'global-utilities.php';
require_once 'sql-utilities.php';
require_once 'map-utilities.php';
require_once 'option-lists.php';


function searchForm() {
	$errors = array();
	$searchFields 
		= textInputField('text', 'Name', 'name', '', '', $errors)
		. selectBox('Suburb', 'suburb', getSuburbOptions())
		. selectBox('Distance', 'distance', getDistanceOptions())
		. selectBox('Rating', 'rating', getRatingOptions())
		. hiddenInputField('user-location', 'location', '');
	
	return genericForm('search.php', 'get', 'Search', $searchFields);
	// '
		// <form action="search.php" method="get" name="search-form">
			// ' . $searchForm . '
			// <input type="submit" name="submit" />
		// </form>
	// ';
}

function getSearchResults() {	
	$name = $_GET['name'];
	$suburb = $_GET['suburb'];
	$rating = $_GET['rating'];
	$distance = $_GET['distance'];
	if (isset($_GET['location']) && $_GET['location'] != '') {		
		$latFrom = explode(",", $_GET['location'])[0];
		$longFrom = explode(",", $_GET['location'])[1];
	}
	
	$pdo = getNewPDO();
	$sql = '
		SELECT id, name, address, suburb, disabledaccess, availability, latitude, longitude, rating, ' . 
			(isset($latFrom) 
				? '(6371 * acos(cos(radians(:latFrom)) 
				   * cos(radians(latitude)) * cos(radians(longitude) - radians(:longFrom)) 
				   + sin(radians(:latFrom)) * sin(radians(latitude))))' 
				: '0') . ' AS distance
		FROM Items
		WHERE ' . ($name == '' ? 'name LIKE "%"' : "name LIKE :name") . '
		AND ' . ($suburb == 'Any' ? 'suburb LIKE "%"' : 'suburb = :suburb') . '
		AND rating >= :rating
		ORDER BY name' .
		(isset($latFrom) && $distance != 'Any' 
			? 'HAVING distance < :distance' 
			: '') . '
	';
	$query = $pdo->prepare($sql);
	if ($name != '') {
		$query->bindValue(':name', "%{$name}%");
	}
	if ($suburb != 'Any') {
		$query->bindValue(':suburb', $suburb);
	}
	if ($rating != 'Any') {
		$query->bindValue(':rating', $rating);
	}
	if (isset($latFrom)) {	
		$query->bindValue(':distance', $distance);
		$query->bindValue(':latFrom', $latFrom);
		$query->bindValue(':longFrom', $longFrom);
	}
	// echo $name;
	// echo $rating;
	// exit();
	$query->execute();
	
	$results = array();
	for ($i = 0; $i < $query->rowCount(); $i++) {
		$results[$i] = $query->fetch();
		if (!isset($latFrom)) {
			$results[$i]['distance'] = 'N/A';
		} else {
			$results[$i]['distance'] = explode('.', $results[$i]['distance'])[0];
		}
	}
	
	return $results;
}

function resultsSection($searchResults) {
	if (count($searchResults) > 0) {
		$itemIDs = array();
		for ($i = 0; $i < count($searchResults); $i++) {
			$itemIDs[$i] = $searchResults[$i]['id'];
		}
		
		echo '
			<div id="search-results">
				<h1>Results</h1>
				<div id="map-canvas">
					<script>
						var mapLocations = ' . json_encode(getMapLocations($itemIDs)) . '
					</script>
				</div>
		';
		if (count($searchResults) > 0) {
			foreach ($searchResults as $item) {
				echo searchResult(
					$item['name'],
					$item['address'] . ', ' . $item['suburb'],
					$item['distance'],
					$item['availability'],
					$item['disabledaccess'],
					$item['rating'],
					'item.php?id=' . $item['id']
				);
			}
		}
		echo '
			</div>
		';
	} else {
		echo '<h1>No Results Found</h2>';
	}
}

function searchResult($itemName, $location, $distance, $availability, $accessibility, $rating, $link) {
	$resultContent = '
		<div>
			<div class="location">
				' . ucwords(strtolower($location)) . '
			</div>
			<div class="quantities">
				<div>' . $distance . 'km</div>
				<div>' . ucwords(strtolower($availability)) . '</div>
				<div >' . ucwords(strtolower($accessibility)) . '</div>
				<div>' . getItemRating($rating) . '</div>
			</div>
		</div>
	';	
	return genericLink(ucwords(strtolower($itemName)), $resultContent, $link, '', '>');
}

?>