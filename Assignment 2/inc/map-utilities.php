<?php

require_once 'sql-utilities.php';

function getMapLocations($ids) {
	$pdo = getNewPDO();
	
	$qMarks = str_repeat('?, ', count($ids) - 1) . '?';
	$query = $pdo->prepare('SELECT id, name, latitude, longitude FROM Items WHERE id IN (' . $qMarks . ')');
	$query->execute($ids);
	
	$items = array();
	for ($i = 0; $i < $query->rowCount(); $i++) {
		$items[$i] = $query->fetch();
	}
	return $items;
}

?>