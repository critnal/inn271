<?php

require_once 'sql-utilities.php';

function uploadCSV() {

	$file = fopen($_FILES['file']['tmp_name'], 'r');	
	$rows = array();
	
	// Get the first row in the file
	// If it's a header/title row then it will be
	// replaced by the while loop
	$count = 1;
	$rows[0] = fgetcsv($file);
	foreach ($rows[0] as $value) {
		if ($value == 'id' || $value == 'suburb') {
			$count = 0;
			break;
		}
	}
	
	while (!feof($file)) {
		$tempRow = fgetcsv($file);
		// Simple check to make sure it doesn't accept empty rows
		if (count($tempRow) > 1) {
			// foreach ($tempRow as $value) {
				// echo $value;
			// }	
			$rows[$count] = $tempRow;
			$count++;
			// echo '<br/>';	
		}		
	}
	
	return insertCSV($rows);
}

function insertCSV($rows) {
	try {
		$pdo = getNewPDO();
		
		// First clear the Items table
		$query = $pdo->prepare(CLEAR_ITEMS_TABLE);
		$query->execute();
		
		// Then insert the new data
		foreach ($rows as $item) {
			$query = $pdo->prepare(INSERT_NEW_ITEM);
			$query->bindValue(":id", $item[0]);
			$query->bindValue(":name", $item[1]);
			$query->bindValue(":place", $item[2]);
			$query->bindValue(":address", $item[3]);
			$query->bindValue(":suburb", $item[4]);
			$query->bindValue(":description", $item[5]);
			$query->bindValue(":disabledaccess", $item[6]);
			$query->bindValue(":availability", $item[7]);
			$query->bindValue(":latitude", $item[8]);
			$query->bindValue(":longitude", $item[9]);
			$query->execute();
		}
	
	} catch (PDOexception $exception) {
		echo $exception->getMessage();
	}
	return true;
}

?>