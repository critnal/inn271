<?php

require_once 'global-utilities.php';
require_once 'sql-utilities.php';
require_once 'review-utilities.php';
require_once 'option-lists.php';

function getItem($id) {	
	$pdo = getNewPDO();
	$query = $pdo->prepare(SELECT_ITEM_ALL);
	$query->bindValue(':id', $id);
	$query->execute();
	
	return $query->fetch();
}

function itemSection($item) {

	return '<h1>' . ucwords(strtolower($item['name'])) . '</h1>'
	. genericBox(
		'Place',
		ucwords(strtolower($item['place'])))
	. genericBox(
		'Address',
		ucwords(strtolower($item['address'])))
	. genericBox(
		'Suburb',
		ucwords(strtolower($item['suburb'])))
	. genericBox(
		'Availability',
		ucwords(strtolower($item['availability'])))
	. genericBox(
		'Handicapped Access',
		$item['disabledaccess'])
	. genericBox(
		'Rating',
		getItemRating($item['rating']))
	. '
		<div id="map-canvas">
			<script>
				var mapLocations = ' . json_encode(getMapLocations(array($item['id']))) . '
			</script>
		</div>
	';
}

function reviewSection($reviews) {
	echo '<h1>Reviews</h1>';
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
		echo reviewForm();
	} else {
		echo genericLink('You must log in to write a review', '', 'account.php', '', '');
	}
	showReviews($reviews);
}

function reviewForm() {
	$array = array();
	$reviewFields
		= textAreaField('Write a Review', 'content', '', '* Invalid text', $array)
		. selectBox('Rating', 'rating', getRatingValues())
		. hiddenInputField('', 'id', $_GET['id']);
	return genericForm('review.php', 'post', 'Submit', $reviewFields);
	
	// $form 
		// = '<form action="review.php" name ="review-form">'
		// . '	<h2>Write a Review</h2>'
		// . 	
		// . '</form';
	// return genericBox('', $form);
	
}

?>