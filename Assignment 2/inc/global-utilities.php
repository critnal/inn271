<?php

function genericHead() {
	return '
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,300italic,400italic" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/global.css" />
		<script src="js/global.js"></script>
	';
}

function navigationMenu($selected) {
	return '
		<div id="navigation">
			<ul id="menu">
				<li '.($selected == "Home" 		? 'class="selected"' : '').'><a href="index.php">Home</a></li>
				<li '.($selected == "Account" 	? 'class="selected"' : '').'><a href="account.php">Account</a></li>
				<li '.($selected == "Recent" 	? 'class="selected"' : '').'><a href="recent.php">Recent</a></li>
				<li '.($selected == "Search" 	? 'class="selected"' : '').'><a href="search.php">Search</a></li>
			</ul>
		</div>
	';
}

function genericLink($heading, $content, $link, $onclick, $icon) {
	return '
		<a href="' . $link . '" class="generic-link" onclick="' . $onclick . '">
			<div>
				<div><h2>' . $heading . '</h2></div>
				<div><h2>><h2></div>
			</div>
			<div>
				' . $content . '
			</div>
		</a>
	';
}

function genericBox($heading, $content) {
	
	return '
		<div class="generic-box">
			<h2>' . $heading . '</h2>
			' . $content . '
		</div>
	';
}

function selectBox($label, $name, $options) {	
	$selectBox = '
		<select name="' . $name . '" id="' . $name . '">';

	foreach ($options as $key => $value) {
		$selectBox .= '
			<option value="' . $value . '">' . $key . '</option>';
	}
	
	$selectBox .= '
		</select>
	';	
	return genericInputField($label, $selectBox);
}

function textInputField($type, $label, $name, $placeholder, $errorMessage, &$errors) {
	$fieldContent = '
		<div>
			<input type="' . $type . '" name="' . $name . '" placeholder="' . $placeholder . '" />';	
	if (isset($errors[$name])) {
		$fieldContent .= '
			<div class="warning" >
			' . $errorMessage . '
			</div>';
	}	
	$fieldContent .= '
		</div>
	';	
	return genericInputField($label, $fieldContent);
}

function textAreaField($label, $name, $placeholder, $errorMessage, &$errors) {
	$fieldContent = '
		<div>
			<textarea name="' . $name . '" placeholder="' . $placeholder . '" rows="4" cols="50"></textarea>';	
	if (isset($errors[$name])) {
		$fieldContent .= '
			<div class="warning" >
			' . $errorMessage . '
			</div>';
	}	
	$fieldContent .= '
		</div>
	';	
	return genericInputField($label, $fieldContent);
}

function hiddenInputField($id, $name, $value) {
	return '
		<div>
			<input type="hidden" id="' . $id . '" name="' . $name . '" value="' . $value . '"/>
		</div>
	';	
}

function genericInputField($label, $content) {	
	return '
		<div class="generic-input-field">
			<h2>' . $label . '</h2>
			' . $content . '
		</div>
	';
}

function genericForm($action, $method, $submitLabel, $content) {
	return '<div>
			<form action="' . $action . '" method="' . $method . '">
				' . $content . '
				<input type="submit" name="submit" value="' . $submitLabel . '" />
			</form>
		</div>
	';
}

?>