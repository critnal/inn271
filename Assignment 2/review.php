<?php

require_once "inc/review-utilities.php";
session_start();

if (isset($_POST['submit']) && isset($_POST['id'])) {
	if (validateReview($_SESSION['username'], $_POST['id'], $_POST['content'], $errors)) {
		enterReview($_SESSION['username'], $_POST['id'], $_POST['content'], $_POST['rating']);
	}
	header('location: item.php?id=' . $_POST['id']);
	
} else {
	header('location: search.php');
}

?>