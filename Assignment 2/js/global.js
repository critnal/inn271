function addDisplayNone(element) {
    if (!element.className.match(/\bdisplay-none\b/)) {
        element.className += " display-none";
    }
}

function removeDisplayNone(element) {
    //window.alert("remove: " + element.className);
    if (element.className.match(/(?:^|\s)display-none(?!\S)/)) {
        element.className =
            element.className.replace(/(?:^|\s)display-none(?!\S)/, '');
    }
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}