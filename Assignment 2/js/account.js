function validateRegistration() {
    // var registrationFields = document.getElementsByClassName('reg-field');
    // var allFieldsAreValid = true;

    // for (var i = 0; i < registrationFields.length; i++) {
        // allFieldsAreValid = 
			// allFieldsAreValid && validateField(registrationFields[i]);
    // }
	// if (allFieldsAreValid) {
		document.getElementById("register-form").submit();
	// }
}

function validateField(fieldElement) {
    var isValid = false;

    if (hasClass(fieldElement, "name-field")) {
        isValid = isValidNameField(fieldElement.value);
    }
    else if (hasClass(fieldElement, "email-field")) {
        isValid = isValidEmailField(fieldElement.value);
    }
    else if (hasClass(fieldElement, "date-field")) {
        isValid = isValidDateField(fieldElement.value);
    }
    else if (hasClass(fieldElement, "password-field")) {
        isValid = isValidPasswordField(fieldElement.value);
    }
    if (fieldElement.id == "reenter-password-data") {
        var password
            = document.getElementById("password-data").value;
        isValid = (fieldElement.value === password);
    }

    if (!isValid) {
        showFieldWarning(fieldElement);
		return false;
    } else {
        removeFieldWarning(fieldElement);
		return true;
    }
}

function isValidNameField(name) {
    return name.match(/^[a-zA-Z]+$/);
    //return false;
}

function isValidEmailField(email) {
    return email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/);
}

function isValidDateField(date) {
    return date.match(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g);
}

function isValidPasswordField(password) {
    return password.match(/^[a-zA-Z0-9]+$/);
}

function showFieldWarning(fieldElement) {
    var warningElement
        = fieldElement.parentElement.getElementsByClassName("warning")[0];
    removeDisplayNone(warningElement);
}

function removeFieldWarning(fieldElement) {
    var warningElement
        = fieldElement.parentElement.getElementsByClassName("warning")[0];
    addDisplayNone(warningElement);
}