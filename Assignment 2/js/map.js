google.maps.event.addDomListener(window, 'load', initialiseMap);
getLocation();

function initialiseMap() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = { 
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
		maxZoom: 18
	};
    var map = new google.maps.Map(mapCanvas, mapOptions);
	var mapBounds = new google.maps.LatLngBounds();

	for (var i = 0; i < mapLocations.length; i++) {
		var item = mapLocations[i];
		
		var location = new google.maps.LatLng(item['latitude'], item['longitude']);
		
		addMapMarker(map, location, item['name'], 'item.php?id=' + item['id']);
		
		mapBounds.extend(location);
	}
	
    map.fitBounds(mapBounds);
	map.panToBounds(mapBounds);
}

function addMapMarker(map, location, title, url) {
	var marker = new google.maps.Marker({
		map: map,
		position: location,
		title: title,
		url: url
	});
	google.maps.event.addListener(marker, 'click', function() {
		window.location.href = marker.url;
	});
}

function getLocation() {
	navigator.geolocation.getCurrentPosition(insertLocation);
}

function insertLocation(position) {
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
	
	var inputElement = document.getElementById("user-location");
	inputElement.value = "" + latitude + "," + longitude;
}