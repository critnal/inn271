﻿<?php 
require_once "inc/global-utilities.php";
session_start();

if (!isset($_SESSION['loggedin'])) {
	header('Location: login.php');
	exit();
}	
?>


<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

    <?php echo genericHead(); ?>

    <!-- Styles that ONLY apply to this page -->
    <link rel="stylesheet" type="text/css" href="css/account.css" />
</head>
<body>
    <div id="container">
	
        <?php echo navigationMenu('Account'); ?>

        <!-- This div is for all page content -->
        <div id="content">                  
			<?php
				echo '<h1>' . $_SESSION['username'] .'</h1>';
				
				echo genericLink('Manage Account', '', '', '', '>');
				if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) {
					echo genericLink('Admin Tools', '', 'admin.php', '', '>');
				}
				echo genericLink('Logout', '', 'logout.php', '', '>');
			?>
        </div>
    </div>
</body>
</html>