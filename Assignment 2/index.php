﻿<?php require_once "inc/global-utilities.php"; ?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

	<?php echo genericHead(); ?>
	
    <!-- Styles that ONLY apply to this page -->
    <link rel="stylesheet" type="text/css" href="css/home.css" />
</head>
<body>
    <div id="container">
	
		<?php echo navigationMenu('Home'); ?>

        <!-- This div is for the front page logo -->
        <div id="header">
            <p>Elegant<br />Lavatories</p>
        </div>
    </div>
</body>
</html>