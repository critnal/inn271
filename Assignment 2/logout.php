<?php
require_once "inc/global-utilities.php";
require_once "inc/account-utilities.php";
session_start();
endUserSession();
?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

    <?php echo genericHead(); ?>
</head>
<body>
    <div id="container">
		<?php echo navigationMenu('Account'); ?>
		<div id="content">
			<h1>Logged out!</h1>
		</div>
    </div>
</body>
</html>