﻿<?php 
require_once "inc/global-utilities.php"; 
require_once "inc/recent-utilities.php"; 
?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

	<?php echo genericHead(); ?>
</head>
<body>
    <div id="container">

        <?php echo navigationMenu('Recent'); ?>

        <!-- This div is for all page content -->
        <div id="content">		
			<?php echo recentReviewsSection(); ?>
        </div>
    </div>
</body>
</html>