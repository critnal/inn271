<?php 
require_once "inc/global-utilities.php";
require_once "inc/account-utilities.php"; 
$errors = array();
if (isset($_POST['username']) && isset($_POST['password'])) {
	performRegistration($errors);
}
?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

    <?php echo genericHead(); ?>

    <!-- Styles that ONLY apply to this page -->
    <link rel="stylesheet" type="text/css" href="css/account.css" />
</head>
<body>
    <div id="container">
	
        <?php echo navigationMenu('Account'); ?>

        <!-- This div is for all page content -->
        <div id="content">    
            <div id="registration">
                <h1>Registration</h1>

                <!-- These are the user registration fields -->
				<form id="register-form" method="post" action="register.php">
				<?php
					$errorMessage = "* Must only use standard ASCII characters";
					if (isset($errors['usernametaken'])) {
						$errorMessage = "* Username already exists";
						$errors['username'] = 1;
					}
					echo textInputField("text", "Username", "username", "", $errorMessage, $errors);
					echo textInputField("text", "First Name", "firstname", "", "* Must only use characters a-Z", $errors);
					echo textInputField("text", "Last Name", "lastname", "", "* Must only use characters a-Z", $errors);
					echo textInputField("text", "Email", "email", "example@domain.com", "* Must be a valid email", $errors);
					echo textInputField("text", "Date of Birth", "dateofbirth", "DD/MM/YYYY", "* Must be DD/MM/YYYY format", $errors);
					echo textInputField("password", "Password", "password", "", "* Must only use characters a-Z and numbers 0-9", $errors);
					echo textInputField("password", "Re-enter Password", "passwordconfirm", "", "* Both password must match", $errors);
					
				?>
					<div class="generic-box">
						<h2 id="subscribe-header">
							Subscribe
						</h2>
						<input type="checkbox" id="subscribe-data" value="1" name="subscribe" /> Newsletter
					</div>
					<a class="generic-button" onclick="document.getElementById('register-form').submit();">
						<h2 class="label">Complete Registration</h2>
						<h2 class="icon">></h2>
					</a>
				</form>
            </div>
        </div>
    </div>
</body>
</html>