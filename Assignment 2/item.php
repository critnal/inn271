﻿<?php 

require_once "inc/global-utilities.php";
require_once "inc/item-utilities.php";
require_once "inc/map-utilities.php";

$item = array();
$reviews = array();
$errors = array();

if (isset($_GET['id'])) {
	$item = getItem($_GET['id']);
	$reviews = getReviewsForItem($_GET['id']);
} else {
	header("location: search.php");
}	
	
?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

	<?php echo genericHead(); ?>

    <!-- Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Google Map elements -->
    <link rel="stylesheet" type="text/css" href="css/map.css" />
    <script src="js/map.js"></script>
</head>
<body>
    <div id="container">
	
        <?php echo navigationMenu(''); ?>

        <!-- This div is for all page content -->
        <div id="content">
			<?php 
			echo itemSection($item); 
			echo reviewSection($reviews);
			?>
        </div>
    </div>
</body>
</html>