<?php 
require_once "inc/global-utilities.php";
require_once "inc/account-utilities.php"; 
session_start();

if (isset($_POST['username']) && isset($_POST['password'])) {
	if (performLogin()) {
		setUserSession();
		header('Location: account.php');
		exit();
	}
}
?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

    <?php echo genericHead(); ?>

    <!-- Styles that ONLY apply to this page -->
    <link rel="stylesheet" type="text/css" href="css/account.css" />
</head>
<body>
    <div id="container">
	
        <?php echo navigationMenu('Account'); ?>

        <!-- This div is for all page content -->
        <div id="content">            

            <div id="login">
                <h1>Login</h1>

                <!-- This is the user login box -->
				<form method="post" action="login.php">
					<div id="login-box">
						<div id="labels">
							<span>Username</span>
							<span>Password</span>
						</div>
						<input class="clickable" id="submit-button" type="submit" value=">"/>
						<div id="inputs">
							<input type="text" name="username" />
							<input type="password" name="password" />
						</div>
					</div>
				</form>

                <h1>or</h1>
                <h1>Not a member?</h1>

                <a href="register.php" class="generic-button">
                    <h2 class="label">Register</h2>
                    <h2 class="icon">></h2>
                </a>

            </div>            

        </div>
    </div>
</body>
</html>