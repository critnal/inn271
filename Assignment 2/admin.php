<?php 
require_once "inc/global-utilities.php";
require_once "inc/admin-utilities.php";
session_start();

if (!isset($_SESSION['admin']) || $_SESSION['admin'] != 1) {
	header('Location: login.php');
	exit();
}	

if (isset($_FILES['file'])) {
	if (uploadCSV()) {
		
	}
}
?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

    <?php echo genericHead(); ?>
</head>
<body>
    <div id="container">
	
        <?php echo navigationMenu('Account'); ?>

        <!-- This div is for all page content -->
        <div id="content">
			<h1>Admin Tools</h1>
			<form action="admin.php" method="post" enctype="multipart/form-data" id="file-form">				
				<?php
				$errors = array();
				echo textInputField('file', 'Upload data set to database', 'file', '', '* Must be a .csv file', $errors);
				// echo genericLink('Upload', '', '', "document.getElementById('file-form').submit()", '');
				
				?>
				<input type="submit" />
			</form>
        </div>
    </div>
</body>
</html>