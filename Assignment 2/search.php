﻿<?php 
require_once "inc/global-utilities.php";
require_once "inc/search-utilities.php"; 
require_once "inc/option-lists.php"; 

$searchResults = array();

if (isset($_GET['submit'])) {
	$searchResults = getSearchResults();
}
?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Elegant Lavatories</title>

	<?php echo genericHead(); ?>

    <!-- Styles that ONLY apply to this page -->
    <link rel="stylesheet" type="text/css" href="css/map.css" />

    <!-- Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Map script to control Google Map elements -->
    <script src="js/map.js"></script>
</head>
<body>
    <div id="container">
	
        <?php echo navigationMenu('Search'); ?>

        <!-- This div is for all page content -->
        <div id="content">

			<?php			
			echo searchForm();			
			if (isset($_GET['submit'])) {
				echo resultsSection($searchResults);
			}
			?>
			
        </div>
    </div>
</body>
</html>