Core Programming Tasks and Functionality
========================================
Search form provides multiple criteria, but not using different input types, e.g., drop down for suburb/rating. No geolocation. Registration page only uses 3 input types. No validation of numeric data format. All error messages are displayed when validation fails - even for fields with valid data. Validation fails even when valid data is entered.
The site design is elegant!

HTML
====
No metadata.

CSS
===
As required.

JavaScript
==========
Insufficient comments.

Add on #1: Maps
===============
Search results page does not include map.

Add on #2: Metadata
===================
This criteria was not included in marks calculation.

Add on #3: Mobile Design
========================
Home screen icon metadata not implemented.

